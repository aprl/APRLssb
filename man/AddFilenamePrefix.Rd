% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/db_util.R
\name{AddFilenamePrefix}
\alias{AddFilenamePrefix}
\title{AddFilenamePrefix}
\usage{
AddFilenamePrefix(prefix, filename)
}
\arguments{
\item{prefix}{prefix}

\item{filename}{file name}
}
\value{
file path
}
\description{
Add prefix to filename.
}
