R package: APRLssb
===

APRL smoothing splines baseline tool. Implements baseline correction of spectra for aerosols collected on PTFE filters as described by Kuzmiakova, Dillner, and Takahama, *Atmos. Meas. Tech.*, 2016.

More generally, the purpose of this package is to provide a framework for

* baseline fitting over a range of model parameters and multiple segments (regions)
* a method for visualizing baselines and evaluation metrics according to choice of model parameters
* export of selected model for use in other programs (e.g., peak fitting)

Subject to substantial modification. Keep up-to-date with `git pull`.

## Installation

First, please see [installation instructions for APRLspec](https://gitlab.com/aprl/APRLspec), on which APRLssb depends.

Install for R (replace `{username}` and `{password}` with account for which access to private repository is granted):

```{r}
> devtools::install_git('https://{username}:{password}@gitlab.com/aprl/APRLssb.git')
```

After installation, get a copy of example files (templates). To copy them into a new folder called "ssb" in your working directory:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('APRLssb', 'example_files', 'ssb')"
$ cd ssb/
```
To copy them into an existing working directory ("." as third argument) - e.g., the current one where you have used baseline correction scripts - you can optionally pass a prefix as the fourth argument to the `CopyPkgFiles` function:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('APRLssb', 'example_files', '.', 'ssb')"
```
This will add a prefix to all the example files in this directory to prevent naming conflicts with files from other packages.

To uninstall, run the command in R:

```
> remove.packages("APRLssb")
```

**Note**: The RSQLite package should be modified as described for [APRLspec's README.md](https://gitlab.com/aprl/APRLspec#rsqliterdb).

## Scripts

### Fit baselines


Fit baselines (each segment separately) for a sequence of parameters (EDF_T):

```
$ Rscript main_fits_db.R testcase/userinput.json
```

Key parameters in .json file:

* `specfile`: list of spectra files
* `samplelistfile`: (optional) file (.json) of samples.
* `param`: sequence of EDF_T values
* `whichsegment`: name of segments to be evaluated (see below)
* `dbconfig`: name of DB specification file (.json)
* `overwrite`: overwrite tables?
* `segmentsfile`: (optional) file containing segment definitions

`specfile` can be one of .rda, .rds, .csv:

* The .rda or .rds file should contain a spectra matrix object, in which each row represents a single spectrum and the sample names are provided in the `rownames` attribute; wavenumbers should be attached as "wavenum" (`attr(x, "wavenum")).
* A .csv file should contain wavenumber values in the first column and spectra in each of the subsequent columns (with sample labels as column labels).

Outputs:

* `{dbname}.sqlite`

The output is a SQL database which contains baselines generated for each spectra and parameter, and additional parameters if they are estimated internally. Current default implementation of the database is [SQLite](https://sqlite.org/). You can inspect the contents of the output file using the [DB Browser for SQLite](http://sqlitebrowser.org/).

### Visualize fits

```
$ Rscript plot_fits.R testcase/userinputs.json
```

### Calculate and plot evaluation metrics

Calculate aggregated metrics (blank absorbance, NAF):

```
$ Rscript eval_fits_db.R testcase/userinputs.json
```
Key parameters in .json file:

* `whichsegment`: name of segments to be evaluated (see below)
* `samples`: name of file containing blank/sample spectra that should be used. When `blank` is defined and `sample` is `true`, all samples not in the blank set are used.

Outputs:

* `{segment}_blankabs.csv`: sample-specific integrated absolute blank absorbance
* `{segment}_naf.csv`: negative analyte fraction


The command for plotting (does not require database) is as follows:

```
$ Rscript eval_plots.R testcase/userinputs.json
```
Key parameters in .json file:

* `whichsegment`: name of segments to be evaluated

Outputs:

* `{segment}_agg_criteria.pdf`: plots of blankabs and NAF


### Export selected segments

For the time being, export spectra as text files for additional evaluation.

```
$ Rscript export_selected_as_rds.R testcase/userinput.json example/selected_spectra.json
```
Inputs:

* `selected`: list of segment names and the corresponding EDF
* `stitch`: method of stitching if more than one segment is selected
* `paramsfile`: the name of the user input file used previously

Outputs:

* `{segment}_spec.rds`: baseline corrected absorbance spectra matrix (load with `readRDS`). Wavenumbers saved in `"wavenum"` attribute.

### Plot selected

```
$ Rscript plot_selected.R testcase/userinput.json
```

## Input files

Configurations primarily defined by a single .json file.

### Additional input files referenced by user .json file

"./" indicates that the file is in the same directory as the userinputs; otherwise it is the relative path name to the working directory (from which the script is called).

* sample list file: "./samplelist.csv".
* DB configuration file: "./dbspec.json"
* segment definition: "./segments.json"
* baseline correction functions: "./splinefunctions.R"

### Segment definitions

Each segment must be provided with a label (name) a function for baseline correction. This comprises the core of the baseline correction task. Endpoints are used for rotation or plotting, but not for baseline correction. Unless provided explicitly by the user, segment definitions used by Kuzmiakova et al. (2016) are used - `"segm1"` (O-H and C-H stretching region) and `"segm2"` (carbonyl region).

## Future work

* improve I/O, e.g.:
  * feed into peakfitting program
  * connect with plswork2 database
  * connect with pmf-tools
* sample-specific parameter selection
* stitching method
* more documentation
