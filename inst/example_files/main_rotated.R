#!/usr/bin/Rscript
rm(list = ls())
setwd("~/Dropbox/APRL_projects")
################################################################################
##
## main_fits_db.R
## Authors: Satoshi Takahama (satoshi.takahama@epfl.ch),
##   Adele Kuzmiakova (adele.kuzmiakova@gmail.com)
## Mar 2016
##
## -----------------------------------------------------------------------------
##
## This file is part of APRLssb
##
## see LICENSE
##
################################################################################

##
## {SCRIPT_DESCRIPTION}
##

defaults <- list(
  "segments" = "$extdata/segmentsKDT.json",
  "dbconfig" = c(
    "driver" = "SQLite",
    "file"   = "./baselinedb.sqlite",
    "tables" = "$extdata/dbspec_bl.json"
  )
)

## -----------------------------------------------------------------------------

options(stringsAsFactors=FALSE)

## -----------------------------------------------------------------------------

library(Rfunctools)
library(APRLspec)
library(APRLssb)
library(RSQLite)
library(RJSONIO)
library(reshape2)
library(dplyr)

## -----------------------------------------------------------------------------

# argv <- ParseArgs()
# userinput <- argv$args[1]
# srcpath <- dirname(argv$file)
# runpath <- dirname(argv$args[1])

userinput <- "ssb/SOAS_APRL_PM1/rotatedSpectra/SOAS_PM1_30C/userinput.json"
srcpath <- "/Users/matteo/Dropbox/Satoshi_Ann~+~collaboration/MatteoShared/shiny/APRLapp/APRLapp/APRLapp/APRLLibrary/APRLssb/inst/example_files"
runpath <- "ssb/SOAS_APRL_PM1/rotatedSpectra/SOAS_PM1_30C"

Path <- ExpandPath(dotslash=runpath, package="APRLssb")

## -----------------------------------------------------------------------------
## User inputs

args <- FillDefaults(fromJSON(userinput), defaults)

## -----------------------------------------------------------------------------

spec <- Reduce(SBind, lapply(Path(args[["specfile"]]), ReadSpec, 
                             ext = args[["specfile_reader"]]))

wavenum <- Wavenumbers(spec)
paramseq <- SetLabel(args[["param"]], "P")

## -----------------------------------------------------------------------------

dbconfig <- args[["dbconfig"]]
dbfile <- Path(dbconfig[["file"]])
dbtables <- fromJSON(Path(dbconfig[["tables"]]))

## -----------------------------------------------------------------------------

segments <- fromJSON(Path(args[["segments"]]))
source(Path(segments[["Rfile"]]))

## source(Pkgfile("FitSpline_kdt.R"))

## -----------------------------------------------------------------------------

if(is.character(args[["samplelistfile"]])) {
  samplelist <- unlist(ReadSamplelist(Path(args[["samplelistfile"]]), "type"), use.names=FALSE)
} else {
  samplelist <- rownames(spec)
}



## ----- extract arguments ------

endpoints <- segments[["segm2ext"]][["endpoints"]]
Fitfn <- get(segments[["segm2ext"]][["fit"]])
jj <- MakeMask(wavenum, endpoints)

spec.rotated_segm2 <- matrix(nrow = dim(spec)[1], ncol = length(wavenum[which(jj)]))
row.names(spec.rotated_segm2) <- row.names(spec)
attr(spec.rotated_segm2, "wavenum") <- wavenum[which(jj)]

for(sample_name in unique(samplelist)) {
  spec.r <- RotateTrans(wavenum, spec[sample_name, ], range(which(jj)))
  spec.rotated_segm2[sample_name, ] <- spec.r[jj]
}

save(spec.rotated_segm2, file = file.path(runpath, "rotated_spectra.rda"))
saveRDS(spec.rotated_segm2, file = file.path(runpath, "rotated_spectra.rds"))





## -----------------------------------------------------------------------------

# for(segm in args[["whichsegment"]]) {
#   
#   ## ----- extract arguments ------
#   
#   endpoints <- segments[[segm]][["endpoints"]]
#   Fitfn <- get(segments[[segm]][["fit"]])
#   jj <- MakeMask(wavenum, endpoints)
#   
#   ## ----- open database ------
#   
#   db <- SpecDB(AddFilenamePrefix(segm, dbfile),
#                dbtables,
#                dbconfig[["driver"]])
#   
#   for(x in dbListTables(db$conn))
#     DropTable(db, x)
#   
#   ## write tables within DB
#   WriteDictTable(db, "wavenumbers", wavenum[jj])
#   WriteDictTable(db, "param_input", paramseq)
#   SetIndexKey(db, "wavenumbers")
#   SetIndexKey(db, "param_input")
#   
#   ## create tables within DB
#   extracolumns <- setNames(rep("DOUBLE", length(wavenum[jj])), names(wavenum[jj]))
#   CreateTable(db, "description")
#   CreateTable(db, "spectra_baseline", extracolumns)
#   CreateTable(db, "spectra_absorbance", extracolumns)
#   CreateTable(db, "param_output")
#   
#   ## ----- baseline correction for each sample -----
#   
#   for(sample in unique(samplelist)) {
#     
#     cat("segment:", segm, "sample:", sample, "\n")
#     
#     ## ----- apply baseline correction -----
#     
#     ## rotate/translate
#     spec.r <- RotateTrans(wavenum, spec[sample,], range(which(jj)))
#     
#     ## baseline correction
#     out <- try(t(sapply(paramseq, Flip(Fitfn, 3), wavenum[jj], spec.r[jj])), TRUE)
#     
#     if(class(out)=="try-error") {
#       print(out)
#       next
#     }
#     
#     ## ----- format output for database -----
#     
#     ## build index data frame
#     primkey <- names(db$tables[["description"]]$columns[["primary"]])
#     grid <- MakeIndexGrid(sample = setNames(,sample), param = names(paramseq), index=primkey)
#     id <- grid[,primkey]
#     
#     ## structure into matrices -> data frames
#     table.bl <- as.data.frame(do.call(rbind, out[,"baseline"]))
#     table.abs <- as.data.frame(do.call(rbind, out[,"absorbance"]))
#     table.param <- as.data.frame(t(apply(out[,c("bounds","param")], 1, unlist)))
#     dimnames(table.bl) <- dimnames(table.abs) <- list(id, names(wavenum[jj]))
#     dimnames(table.param) <- list(id, names(db$tables[["param_output"]]$columns$values))
#     
#     ## ----- save to database -----
#     
#     AppendRows(db, "description", grid)
#     AppendRows(db, "spectra_baseline", ResetIndex(table.bl, primkey))
#     AppendRows(db, "spectra_absorbance", ResetIndex(table.abs, primkey))
#     AppendRows(db, "param_output", ResetIndex(table.param, primkey))
#     
#   }
#   
#   Close(db)
#   
# }
